# Lutim language file
# Copyright (C) 2014 Luc Didry
# This file is distributed under the same license as the Lutim package.
# 
# Translators:
# Translators:
# Thor77 <thor77@thor77.org>, 2015
# Luc Didry <luc@framasoft.org>, 2018. #zanata
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: YEAR-MO-DA HO:MI+ZONE\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"PO-Revision-Date: 2018-03-11 08:51+0000\n"
"Last-Translator: Copied by Zanata <copied-by-zanata@zanata.org>\n"
"Language-Team: German (http://www.transifex.com/fiat-tux/lutim/language/de/)\n"
"Language: de\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Zanata 4.3.3\n"

#. (7)
#. (30)
#. ($delay)
#. (config('max_delay')
#: lib/Lutim/Command/cron/stats.pm:149 lib/Lutim/Command/cron/stats.pm:150 lib/Lutim/Command/cron/stats.pm:160 lib/Lutim/Command/cron/stats.pm:161 lib/Lutim/Command/cron/stats.pm:177 lib/Lutim/Command/cron/stats.pm:178 themes/default/templates/partial/for_my_delay.html.ep:13 themes/default/templates/partial/for_my_delay.html.ep:14 themes/default/templates/partial/for_my_delay.html.ep:4 themes/default/templates/partial/lutim.js.ep:142 themes/default/templates/partial/lutim.js.ep:151 themes/default/templates/partial/lutim.js.ep:152 themes/default/templates/raw.html.ep:19 themes/default/templates/raw.html.ep:20 themes/default/templates/raw.html.ep:36 themes/default/templates/raw.html.ep:37 themes/default/templates/raw.html.ep:8 themes/default/templates/raw.html.ep:9
msgid "%1 days"
msgstr "%1 Tage"

#. ($total)
#: themes/default/templates/stats.html.ep:2
msgid "%1 sent images on this instance from beginning."
msgstr "%1 Bilder wurden bisher über diese Instanz versendet."

#: themes/default/templates/index.html.ep:190
msgid "-or-"
msgstr "-oder-"

#: lib/Lutim.pm:187 lib/Lutim/Command/cron/stats.pm:151 lib/Lutim/Command/cron/stats.pm:162 lib/Lutim/Command/cron/stats.pm:179 themes/default/templates/index.html.ep:5 themes/default/templates/raw.html.ep:10 themes/default/templates/raw.html.ep:21 themes/default/templates/raw.html.ep:38
msgid "1 year"
msgstr "1 Jahr"

#: lib/Lutim.pm:186 lib/Lutim/Command/cron/stats.pm:148 lib/Lutim/Command/cron/stats.pm:159 lib/Lutim/Command/cron/stats.pm:176 themes/default/templates/index.html.ep:4 themes/default/templates/partial/for_my_delay.html.ep:13 themes/default/templates/partial/lutim.js.ep:151 themes/default/templates/raw.html.ep:18 themes/default/templates/raw.html.ep:35 themes/default/templates/raw.html.ep:7
msgid "24 hours"
msgstr "24 Stunden"

#: themes/default/templates/partial/myfiles.js.ep:57
msgid ": Error while trying to get the counter."
msgstr ":Fehler beim Abrufen des Zählers."

#: themes/default/templates/partial/navbar.html.ep:77
msgid "About"
msgstr ""

#: lib/Lutim/Command/cron/stats.pm:144 themes/default/templates/raw.html.ep:3
msgid "Active images"
msgstr ""

#: lib/Lutim/Controller.pm:326
msgid "An error occured while downloading the image."
msgstr "Beim Herunterladen des Bildes ist ein Fehler aufgetreten."

#: themes/default/templates/zip.html.ep:2
msgid "Archives download"
msgstr ""

#: themes/default/templates/about.html.ep:44 themes/default/templates/myfiles.html.ep:64 themes/default/templates/stats.html.ep:25
msgid "Back to homepage"
msgstr "Zurück zur Hauptseite"

#: themes/default/templates/index.html.ep:193 themes/default/templates/index.html.ep:194
msgid "Click to open the file browser"
msgstr "Klicken um den Dateibrowser zu öffnen"

#: themes/default/templates/gallery.html.ep:27
msgid "Close (Esc)"
msgstr ""

#: themes/default/templates/about.html.ep:30
msgid "Contributors"
msgstr "Mitwirkende"

#: themes/default/templates/partial/lutim.js.ep:218 themes/default/templates/partial/lutim.js.ep:272 themes/default/templates/partial/lutim.js.ep:350
msgid "Copy all view links to clipboard"
msgstr "Alle Links zum Anschauen in die Zwischenablage kopieren"

#: themes/default/templates/index.html.ep:18 themes/default/templates/index.html.ep:36 themes/default/templates/index.html.ep:69 themes/default/templates/index.html.ep:77 themes/default/templates/index.html.ep:85 themes/default/templates/index.html.ep:93 themes/default/templates/myfiles.html.ep:20 themes/default/templates/myfiles.html.ep:38 themes/default/templates/partial/common.js.ep:150 themes/default/templates/partial/lutim.js.ep:108 themes/default/templates/partial/lutim.js.ep:123 themes/default/templates/partial/lutim.js.ep:82 themes/default/templates/partial/lutim.js.ep:94
msgid "Copy to clipboard"
msgstr "In die Zwischenablage kopieren"

#: themes/default/templates/myfiles.html.ep:52
msgid "Counter"
msgstr "Zähler"

#: themes/default/templates/stats.html.ep:18
msgid "Delay repartition chart for disabled images"
msgstr ""

#: themes/default/templates/stats.html.ep:15
msgid "Delay repartition chart for enabled images"
msgstr ""

#: themes/default/templates/index.html.ep:115 themes/default/templates/index.html.ep:147 themes/default/templates/index.html.ep:178 themes/default/templates/myfiles.html.ep:53 themes/default/templates/partial/lutim.js.ep:163
msgid "Delete at first view?"
msgstr "Nach erstem Aufruf löschen?"

#: lib/Lutim/Command/cron/stats.pm:145 themes/default/templates/raw.html.ep:4
msgid "Deleted images"
msgstr ""

#: lib/Lutim/Command/cron/stats.pm:146 themes/default/templates/raw.html.ep:5
msgid "Deleted images in 30 days"
msgstr ""

#: themes/default/templates/index.html.ep:98 themes/default/templates/myfiles.html.ep:56 themes/default/templates/partial/common.js.ep:142 themes/default/templates/partial/common.js.ep:145
msgid "Deletion link"
msgstr "Link zum Löschen"

#: themes/default/templates/gallery.html.ep:8
msgid "Download all images"
msgstr "Laden Sie alle Bilder"

#: themes/default/templates/index.html.ep:81 themes/default/templates/index.html.ep:83 themes/default/templates/partial/lutim.js.ep:100 themes/default/templates/partial/lutim.js.ep:104
msgid "Download link"
msgstr "Link zum Herunterladen"

#: themes/default/templates/index.html.ep:28 themes/default/templates/index.html.ep:31 themes/default/templates/myfiles.html.ep:30 themes/default/templates/myfiles.html.ep:33
msgid "Download zip link"
msgstr "Link zum Archivbilder"

#: themes/default/templates/index.html.ep:189
msgid "Drag & drop images here"
msgstr "Bilder hierher ziehen"

#: themes/default/templates/about.html.ep:7
msgid ""
"Drag and drop an image in the appropriate area or use the traditional way to "
"send files and Lutim will provide you four URLs. One to view the image, an "
"other to directly download it, one you can use on social networks and a last "
"to delete the image when you want."
msgstr ""
"Ziehe Bilder in den dafür vorgesehenen Bereich und Lutim wird vier URLs "
"generieren. Eine zum Anschauen, eine zum direkten Herunterladen, eine zum "
"Nutzen in sozialen Netzwerken und eine letzte um das Bild zu löschen."

#: themes/default/templates/index.html.ep:150 themes/default/templates/index.html.ep:181
msgid "Encrypt the image (Lutim does not keep the key)."
msgstr "Verschlüssle das Bild (Lutim behält den Key nicht)"

#: themes/default/templates/partial/lutim.js.ep:47
msgid "Error while trying to modify the image."
msgstr "Beim bearbeiten des Bildes ist ein Fehler aufgetreten."

#: themes/default/templates/stats.html.ep:10
msgid "Evolution of total files"
msgstr "Entwicklung der Anzahl an Dateien"

#: themes/default/templates/myfiles.html.ep:55
msgid "Expires at"
msgstr "Läuft ab am"

#: themes/default/templates/myfiles.html.ep:50
msgid "File name"
msgstr "Dateiname"

#: themes/default/templates/about.html.ep:24
msgid ""
"For more details, see the <a href=\"https://framagit.org/luc/"
"lutim\">homepage of the project</a>."
msgstr ""
"Besuche für mehr Details die <a href=\"https://framagit.org/luc/"
"lutim\">Homepage des Projekts</a>."

#: themes/default/templates/partial/navbar.html.ep:80
msgid "Fork me!"
msgstr "Fork me!"

#: themes/default/templates/index.html.ep:10 themes/default/templates/index.html.ep:13 themes/default/templates/myfiles.html.ep:12 themes/default/templates/myfiles.html.ep:15
msgid "Gallery link"
msgstr "Link zur Galerie"

#: themes/default/templates/partial/common.js.ep:104 themes/default/templates/partial/common.js.ep:87
msgid "Hit Ctrl+C, then Enter to copy the short link"
msgstr "Drücke STRG+C und dann Enter um den Kurz-Link zu kopieren."

#: themes/default/templates/layouts/default.html.ep:54
msgid "Homepage"
msgstr "Webseite"

#: themes/default/templates/about.html.ep:20
msgid "How do you pronounce Lutim?"
msgstr "Wie spricht man Lutim aus?"

#: themes/default/templates/about.html.ep:6
msgid "How does it work?"
msgstr "Wie funktionert das?"

#: themes/default/templates/about.html.ep:18
msgid "How to report an image?"
msgstr "Wie kann ich ein Bild melden?"

#: themes/default/templates/about.html.ep:14
msgid ""
"If the files are deleted if you ask it while posting it, their SHA512 "
"footprint are retained."
msgstr ""
"Wenn du versuchst, ein Bild während dem Hochladen zu löschen, wird die "
"SHA512-Summe des Bildes behalten."

#: themes/default/templates/index.html.ep:163 themes/default/templates/index.html.ep:203
msgid "Image URL"
msgstr "Bild-URL"

#: lib/Lutim/Command/cron/stats.pm:143 themes/default/templates/raw.html.ep:2
msgid "Image delay"
msgstr ""

#: lib/Lutim/Controller.pm:748
msgid "Image not found."
msgstr "Bild nicht gefunden"

#: themes/default/templates/partial/navbar.html.ep:69
msgid "Informations"
msgstr "Informationen"

#: themes/default/templates/partial/navbar.html.ep:33
msgid "Install webapp"
msgstr "Installiere die Webapp"

#: themes/default/templates/partial/navbar.html.ep:29
msgid "Instance's statistics"
msgstr ""

#: themes/default/templates/about.html.ep:11
msgid "Is it really anonymous?"
msgstr "Ist es wirklich anonym?"

#: themes/default/templates/about.html.ep:9
msgid "Is it really free (as in free beer)?"
msgstr "Is es wirklich kostenlos?"

#: themes/default/templates/about.html.ep:21
msgid ""
"Juste like you pronounce the French word <a href=\"https://fr.wikipedia.org/"
"wiki/Lutin\">lutin</a> (/ly.tɛ̃/)."
msgstr ""
"Genauso wie das französische Wort <a href=\"https://fr.wikipedia.org/wiki/"
"Lutin\">lutin</a> (/ly.tɛ̃/)."

#: themes/default/templates/index.html.ep:153 themes/default/templates/index.html.ep:184
msgid "Keep EXIF tags"
msgstr "Behalte EXIF-Daten"

#: themes/default/templates/partial/navbar.html.ep:40
msgid "Language"
msgstr ""

#: themes/default/templates/index.html.ep:118 themes/default/templates/index.html.ep:166 themes/default/templates/index.html.ep:206 themes/default/templates/partial/lutim.js.ep:167
msgid "Let's go!"
msgstr "Los gehts!"

#: themes/default/templates/partial/navbar.html.ep:74
msgid "License:"
msgstr "Lizenz:"

#: themes/default/templates/index.html.ep:89 themes/default/templates/index.html.ep:91 themes/default/templates/partial/lutim.js.ep:114 themes/default/templates/partial/lutim.js.ep:118
msgid "Link for share on social networks"
msgstr "Links zum teilen auf sozialen Netzwerken"

#: themes/default/templates/zip.html.ep:7
msgid ""
"Lutim can't zip so many images at once, so it splitted your demand in "
"multiple URLs."
msgstr ""

#: themes/default/templates/about.html.ep:4
msgid ""
"Lutim is a free (as in free beer) and anonymous image hosting service. It's "
"also the name of the free (as in free speech) software which provides this "
"service."
msgstr ""
"Lutim ist ein freier und anonymer Bilder-Upload-Service.\n"
"Es ist auch der Name der freien Software, die diesen Service bietet."

#: themes/default/templates/about.html.ep:25
msgid "Main developers"
msgstr "Haupt-Entwickler"

#: themes/default/templates/index.html.ep:73 themes/default/templates/index.html.ep:75 themes/default/templates/partial/lutim.js.ep:88 themes/default/templates/partial/lutim.js.ep:91
msgid "Markdown syntax"
msgstr "Markdown Syntax"

#: themes/default/templates/myfiles.html.ep:2 themes/default/templates/partial/navbar.html.ep:26
msgid "My images"
msgstr "Meine Bilder"

#: themes/default/templates/gallery.html.ep:43
msgid "Next (arrow right)"
msgstr ""

#: themes/default/templates/partial/myfiles.js.ep:19
msgid "No limit"
msgstr "Keine Begrenzung"

#: themes/default/templates/index.html.ep:165 themes/default/templates/index.html.ep:198
msgid "Only images are allowed"
msgstr "Es sind nur Bilder erlaubt"

#: themes/default/templates/myfiles.html.ep:6
msgid ""
"Only the images sent with this browser will be listed here. The details are "
"stored in localStorage: if you delete your localStorage data, you'll loose "
"these details."
msgstr ""
"Nur die Bilder, die über diesen Browser versendet wurden, werden hier "
"angezeigt. Die Details werden im localStorage gespeichert, wenn du diesen "
"leerst, sind die Details verloren."

#: themes/default/templates/about.html.ep:16
msgid ""
"Only the uploader! (well, only if he's the only owner of the images' rights "
"before the upload)"
msgstr ""
"Nur der Hochladende (natürlich nur, wenn er vorher auch Rechteinhaber des "
"Bildes war)"

#: themes/default/templates/zip.html.ep:12
msgid "Please click on each URL to download the different zip files."
msgstr ""

#. (config('contact')
#: themes/default/templates/about.html.ep:19
msgid "Please contact the administrator: %1"
msgstr "Kontaktiere den Administrator: %1"

#: themes/default/templates/gallery.html.ep:41
msgid "Previous (arrow left)"
msgstr ""

#: themes/default/templates/stats.html.ep:22
msgid "Raw stats"
msgstr ""

#: themes/default/templates/index.html.ep:158
msgid "Send an image"
msgstr "Sende ein Bild"

#: themes/default/templates/partial/lutim.js.ep:23
msgid "Share it!"
msgstr "Teile es!"

#: themes/default/templates/index.html.ep:133 themes/default/templates/partial/gallery.js.ep:211 themes/default/templates/partial/lutim.js.ep:178
msgid "Something bad happened"
msgstr "Es ist ein Fehler aufgetreten"

#. ($c->config('contact')
#: lib/Lutim/Controller.pm:755
msgid ""
"Something went wrong when creating the zip file. Try again later or contact "
"the administrator (%1)."
msgstr ""
"Es ist ein Fehler aufgetreten. Versuche es erneut oder kontaktiere den "
"Administrator (%1)."

#: themes/default/templates/partial/navbar.html.ep:52
msgid "Support the author"
msgstr ""

#: themes/default/templates/partial/navbar.html.ep:60
msgid "Support the author on Liberapay"
msgstr ""

#: themes/default/templates/partial/navbar.html.ep:57
msgid "Support the author on Tipeee"
msgstr ""

#: themes/default/templates/partial/navbar.html.ep:63
msgid "Support the author with bitcoins"
msgstr ""

#: themes/default/templates/about.html.ep:13
msgid ""
"The IP address of the image's sender is retained for a delay which depends "
"of the administrator's choice (for the official instance, which is located "
"in France, it's one year)."
msgstr ""
"Die IP-Adresse des Nutzers wird für eine bestimmte Zeit gespeichert. Diese "
"kann der Administrator frei wählen (für die offizielle Instanz, die in "
"Frankreich gehostet ist, liegt diese Zeit bei einem Jahr)"

#: themes/default/templates/about.html.ep:23
msgid ""
"The Lutim software is a <a href=\"http://en.wikipedia.org/wiki/"
"Free_software\">free software</a>, which allows you to download and install "
"it on you own server. Have a look at the <a href=\"https://www.gnu.org/"
"licenses/agpl-3.0.html\">AGPL</a> to see what you can do."
msgstr ""
"Lutim ist <a href=\"https://de.wikipedia.org/wiki/Freie_Software\">freie "
"Software</a>, was dir erlaubt sie herunterzuladen und sie auf deinem eigenem "
"Server zu installieren. Schau dir die <a href=\"https://www.gnu.org/licenses/"
"agpl-3.0.html\">AGPL</a> an um deine Recht zu sehen."

#: lib/Lutim/Controller.pm:345
msgid "The URL is not valid."
msgstr "Die URL ist nicht gültig."

#: themes/default/templates/zip.html.ep:16
msgid ""
"The automatic download process will open a tab in your browser for each link."
" You need to allow popups for Lutim."
msgstr ""

#: lib/Lutim/Controller.pm:158 lib/Lutim/Controller.pm:226
msgid "The delete token is invalid."
msgstr "Das Token zum Löschen ist ungültig."

#. ($upload->filename)
#: lib/Lutim/Controller.pm:486
msgid "The file %1 is not an image."
msgstr "Die Datei %1 ist kein Bild."

#. ($tx->res->max_message_size)
#. ($c->req->max_message_size)
#. (config('max_file_size')
#: lib/Lutim/Controller.pm:309 lib/Lutim/Controller.pm:378 themes/default/templates/partial/lutim.js.ep:244
msgid "The file exceed the size limit (%1)"
msgstr "Die Datei überschreitet die Größenbeschränkung (%1)"

#: themes/default/templates/stats.html.ep:12
msgid "The graph's datas are not updated in real-time."
msgstr "Die Daten des Graphs werden nicht in Echtzeit aktualisiert."

#. ($image->filename)
#: lib/Lutim/Controller.pm:228
msgid "The image %1 has already been deleted."
msgstr "Das Bild %1 wurde schon gelöscht."

#. ($image->filename)
#: lib/Lutim/Controller.pm:237 lib/Lutim/Controller.pm:242
msgid "The image %1 has been successfully deleted"
msgstr "Das Bild %1 wurde erfolgreich gelöscht."

#: lib/Lutim/Controller.pm:166
msgid "The image's delay has been successfully modified"
msgstr "Die Zeit bis zum Löschen des Bildes wurde erfolgreich geändert."

#: themes/default/templates/index.html.ep:45
msgid "The images are encrypted on the server (Lutim does not keep the key)."
msgstr ""
"Die Bilder werden auf dem Server verschlüsselt (Lutim behält den Key nicht)"

#: themes/default/templates/about.html.ep:5
msgid ""
"The images you post on Lutim can be stored indefinitely or be deleted at "
"first view or after a delay selected from those proposed."
msgstr ""
"Die Bilder, die du auf Lutim hochlädst, können entweder nie, nach dem ersten "
"Aufruf oder nach einer bestimmten Zeit gelöscht werden."

#. ($c->config->{contact})
#: lib/Lutim/Controller.pm:483
msgid "There is no more available URL. Retry or contact the administrator. %1"
msgstr ""
"Es sind keine URLs mehr verfügbar. Versuche es erneut oder kontaktiere den "
"Administrator. %1"

#: themes/default/templates/gallery.html.ep:28
msgid "Toggle fullscreen"
msgstr ""

#: themes/default/templates/partial/navbar.html.ep:16
msgid "Toggle navigation"
msgstr ""

#: lib/Lutim/Command/cron/stats.pm:152 themes/default/templates/raw.html.ep:11
msgid "Total"
msgstr ""

#: themes/default/templates/index.html.ep:60 themes/default/templates/partial/lutim.js.ep:17
msgid "Tweet it!"
msgstr "Twittere es!"

#. ($short)
#: lib/Lutim/Controller.pm:106 lib/Lutim/Controller.pm:200 lib/Lutim/Controller.pm:271
msgid "Unable to find the image %1."
msgstr "Konnte das Bild %1 nicht finden."

#: lib/Lutim/Controller.pm:572 lib/Lutim/Controller.pm:617 lib/Lutim/Controller.pm:654 lib/Lutim/Controller.pm:697 lib/Lutim/Controller.pm:709 lib/Lutim/Controller.pm:720 lib/Lutim/Controller.pm:745 lib/Lutim/Plugin/Helpers.pm:84
msgid "Unable to find the image: it has been deleted."
msgstr "Dieses Bild wurde gelöscht."

#: lib/Lutim/Controller.pm:143
msgid "Unable to get counter"
msgstr "Konnte den Zähler nicht abrufen"

#: themes/default/templates/about.html.ep:17
msgid ""
"Unlike many image sharing services, you don't give us rights on uploaded "
"images."
msgstr ""
"Im Gegensatz zu anderen Bild-Hosting-Diensten, überträgst du uns nicht die "
"Rechte an hochgeladenen Bildern."

#: themes/default/templates/index.html.ep:162 themes/default/templates/index.html.ep:201
msgid "Upload an image with its URL"
msgstr "Lade ein Bild über seine URL hoch"

#: themes/default/templates/myfiles.html.ep:54
msgid "Uploaded at"
msgstr "Hochgeladen am"

#: themes/default/templates/stats.html.ep:6
msgid "Uploaded files by days"
msgstr "Hochgeladene Bilder pro Tag"

#. ($c->app->config('contact')
#: lib/Lutim/Plugin/Helpers.pm:183
msgid ""
"Uploading is currently disabled, please try later or contact the "
"administrator (%1)."
msgstr ""
"Hochladen ist momentan deaktiviert. Versuche es später erneut oder "
"kontaktiere den Administrator (%1)."

#: themes/default/templates/index.html.ep:65 themes/default/templates/index.html.ep:67 themes/default/templates/myfiles.html.ep:51 themes/default/templates/partial/lutim.js.ep:74 themes/default/templates/partial/lutim.js.ep:78
msgid "View link"
msgstr "Link ansehen"

#: themes/default/templates/about.html.ep:22
msgid "What about the software which provides the service?"
msgstr "Welche Software stellt diesen Dienst bereit?"

#: themes/default/templates/about.html.ep:3
msgid "What is Lutim?"
msgstr "Was ist Lutim?"

#: themes/default/templates/about.html.ep:15
msgid "Who owns rights on images uploaded on Lutim?"
msgstr "Wer hat die Rechte an auf Lutim hochgeladenen Bildern?"

#: themes/default/templates/about.html.ep:12
msgid ""
"Yes, it is! On the other side, for legal reasons, your IP address will be "
"stored when you send an image. Don't panic, it is normally the case of all "
"sites on which you send files!"
msgstr ""
"Ja, ist es! Auf der anderen Seite wird deine IP-Adresse, wegen rechtlichen "
"Gründen, beim hochladen gespeichert. Keine Panik, das ist normalerweise der "
"Fall für alle Seiten, an die du Dateien sendest!"

#: themes/default/templates/about.html.ep:10
msgid ""
"Yes, it is! On the other side, if you want to support the developer, you can "
"do it via <a href=\"https://www.tipeee.com/fiat-tux\">Tipeee</a> or via <a "
"href=\"https://liberapay.com/sky/\">Liberapay</a>."
msgstr ""
"Ja, ist es! Auf der anderen Seite kannst du den Entwickler via <a href="
"\"https://www.tipeee.com/fiat-tux\">Tipeee</a> oder <a href=\"https://"
"liberapay.com/sky/\">Liberapay</a> unterstützen."

#: themes/default/templates/zip.html.ep:6
msgid "You asked to download a zip archive for too much files."
msgstr ""

#: themes/default/templates/about.html.ep:8
msgid ""
"You can, optionally, request that the image(s) posted on Lutim to be deleted "
"at first view (or download) or after the delay selected from those proposed."
msgstr ""
"Du kannst Bilder, die du auf Lutim hochlädst, entweder nach dem ernsten "
"Ansehen (oder Herunterladen) oder nach einem der vorgeschlagenen Zeiten "
"löschen lassen."

#: themes/default/templates/gallery.html.ep:29
msgid "Zoom in/out"
msgstr ""

#: themes/default/templates/about.html.ep:27
msgid "and on"
msgstr "und auf"

#: themes/default/templates/about.html.ep:40
msgid "arabic translation"
msgstr ""

#: themes/default/templates/about.html.ep:27
msgid "core developer"
msgstr "Haupt-Entwickler"

#: lib/Lutim.pm:185 lib/Lutim/Command/cron/stats.pm:147 lib/Lutim/Command/cron/stats.pm:158 lib/Lutim/Command/cron/stats.pm:175 themes/default/templates/index.html.ep:3 themes/default/templates/raw.html.ep:17 themes/default/templates/raw.html.ep:34 themes/default/templates/raw.html.ep:6
msgid "no time limit"
msgstr "keine Zeit-Begrenzung"

#: themes/default/templates/about.html.ep:38
msgid "occitan translation"
msgstr "okzitanisch Übersetzung"

#: themes/default/templates/about.html.ep:27
msgid "on"
msgstr "auf"

#: themes/default/templates/about.html.ep:39
msgid "paste image to upload ability"
msgstr ""

#: themes/default/templates/about.html.ep:41
msgid "russian translation"
msgstr "russische Übersetzung"

#: themes/default/templates/about.html.ep:36
msgid "spanish translation"
msgstr "spanische Übersetzung"

#: themes/default/templates/about.html.ep:28
msgid "webapp developer"
msgstr "Webapp-Entwickler"
